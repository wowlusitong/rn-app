import React from 'react';
import {StyleSheet, View, ListView} from 'react-native';
import Blog from './src/components/Blog';

export default class App extends React.Component {

  render() {
    return (
      <View style={styles.container}>
        <Blog />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
    height: '100%',
    backgroundColor: '#F4F4F4'
  },
});
