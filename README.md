# 使用方法

- 修改默认API接口地址
将 package.json 中 SERVER_API 修改为当前接口服务正确地址(默认是http://localhost:8000)，比如改成
```
"start": "REACT_NATIVE_API_SERVER=http://192.168.2.10:8000 react-native-scripts start",
```
- 启动服务
```
yarn start 或者 npm start
```
