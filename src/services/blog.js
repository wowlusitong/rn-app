export async function getBlogs() {
  try {
    const res = await fetch(`${process.env.REACT_NATIVE_API_SERVER}/blogs/`);
    const json = await res.json();
    return json;
  } catch (err) {
    console.error('get blogs', err);
    return [];
  }
}
