import React from 'react';
import {StyleSheet, Text, View, ListView, FlatList} from 'react-native';

import {getBlogs} from '../services/blog';

export default class Blog extends React.Component {
  constructor() {
    super();
    this.state = {
      dataSource: []
    }
  }

  componentDidMount() {
    getBlogs().then(blogs => {
      this.setState({
        dataSource: blogs.map(blog => Object.assign({key: blog.id}, blog))
      })
    })
  }

  renderItem({item}) {
    return (
      <View style={styles.blog}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.number}>点击量: {item.number}</Text>
      </View>
    )
  }

  render() {
    const {dataSource} = this.state;
    return (
      <FlatList
        data={dataSource}
        renderItem={this.renderItem.bind(this)}
        />
    );
  }
}

const styles = StyleSheet.create({
  blog: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fff',
    marginVertical: 5,
    padding: 10
  },
  title: {
    flex: 7,
  },
  number: {
    flex: 2,
    marginLeft: 5,
    color: '#45AAFC'
  }
});
